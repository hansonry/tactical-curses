#include <stdlib.h>
#include "PawnList.h"

#include "List.h"

LIST_MAKE(pawn, struct pawn)

struct pawn_list
{
   struct list_pawn list;
};


struct pawn_list * PawnList_New(void)
{
   struct pawn_list * list = malloc(sizeof(struct pawn_list));
   List_Init_pawn(&list->list, 0, 0);
   return list;
}

void PawnList_Free(struct pawn_list * list)
{
   List_Free_pawn(&list->list);
   free(list);
}

static inline
int PawnList_GetNextNumberForTeam(struct pawn_list * list, int team)
{
   int next = 0;
   size_t count, i;
   struct pawn * pawns = List_Get_pawn(&list->list, &count);
   for(i = 0; i < count; i ++)
   {
      if(pawns[i].team == team && pawns[i].number >= next)
      {
         next = pawns[i].number + 1;
      }
   }
   return next;
}

void PawnList_Add(struct pawn_list * list, int team, int x, int y)
{
   int next = PawnList_GetNextNumberForTeam(list, team);
   struct pawn * pawn = List_AddEmpty_pawn(&list->list, NULL);
   Pawn_Init(pawn);
   pawn->team = team;
   pawn->number = next;
   Vec2i_Set(&pawn->position, x, y);
   
}

size_t PawnList_GetSize(const struct pawn_list * list)
{
   return List_GetCount_pawn(&list->list);
}

const struct pawn * PawnList_GetConstByIndex(const struct pawn_list * list, 
                                             size_t index)
{
   size_t count;
   const struct pawn * pawns = List_GetC_pawn(&list->list, &count);
   if(index < count)
   {
      return &pawns[index];
   }
   return NULL;
}
struct pawn * PawnList_GetByIndex(struct pawn_list * list, 
                                  size_t index)
{
   size_t count;
   struct pawn * pawns = List_Get_pawn(&list->list, &count);
   if(index < count)
   {
      return &pawns[index];
   }
   return NULL;
}

void PawnList_UpdatePawns(struct pawn_list * list, const struct map * map)
{
   size_t count, i;
   struct pawn * pawns = List_Get_pawn(&list->list, &count);
   for(i = 0; i < count; i++)
   {
      Pawn_UpdateView(&pawns[i], map);
   }
}

