#include <ncurses.h>
#include "PawnListRenderer.h"

static inline
void PawnListRenderer_RenderPawns(const struct pawn_list * list,
                                  const struct camera * camera,
                                  int teamView)
{
   (void) teamView;
   size_t i, count = PawnList_GetSize(list);
   for(i = 0; i < count; i ++)
   {
      const struct pawn * pawn = PawnList_GetConstByIndex(list, i);
      int x = pawn->position.x - camera->position.x + camera->screenCenter.x;
      int y = pawn->position.y - camera->position.y + camera->screenCenter.y;
      mvprintw(y, x, "@");
   }
}

static inline
void PawnListRenderer_RenderSingleView(const struct pawn * pawn,
                                       const struct camera * camera)
{
   const struct pawnView * view = &pawn->view;
   size_t vx, vy, index = 0;
   for(vy = 0; vy < view->sideLength; vy ++)
   {
      for(vx = 0; vx < view->sideLength; vx++)
      {
         if(view->data[index])
         {
            int x = pawn->position.x + vx - view->center;
            int y = pawn->position.y + vy - view->center;
            int sx = x - camera->position.x + camera->screenCenter.x;
            int sy = y - camera->position.y + camera->screenCenter.y;
            mvprintw(sy, sx, ".");
         }
         index ++;
      }
   }
}
                                           

static inline
void PawnListRenderer_RenderViews(const struct pawn_list * list, 
                                  const struct camera * camera,
                                  int teamView)
{
   (void) teamView;
   size_t i, count = PawnList_GetSize(list);
   for(i = 0; i < count; i ++)
   {
      const struct pawn * pawn = PawnList_GetConstByIndex(list, i);
      PawnListRenderer_RenderSingleView(pawn, camera);
   }
}


void PawnListRenderer_Render(const struct pawn_list * list, 
                             const struct camera * camera,
                             int teamView)
{
   PawnListRenderer_RenderViews(list, camera, teamView);
   PawnListRenderer_RenderPawns(list, camera, teamView);
 
}

