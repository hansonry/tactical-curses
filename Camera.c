#include <ncurses.h>
#include "Camera.h"

void Camera_Update(struct camera * camera)
{
   getmaxyx(stdscr, camera->screenHeight, camera->screenWidth);
   Vec2i_Set(&camera->screenCenter, camera->screenWidth / 2, 
                                    camera->screenHeight / 2);
}

