#ifndef __PAWN_LIST_H__
#define __PAWN_LIST_H__
#include <stddef.h>
#include "Pawn.h"
#include "Map.h"

struct pawn_list;

struct pawn_list * PawnList_New(void);

void PawnList_Free(struct pawn_list * list);


void PawnList_Add(struct pawn_list * list, int team, int x, int y);


size_t PawnList_GetSize(const struct pawn_list * list);
const struct pawn * PawnList_GetConstByIndex(const struct pawn_list * list, 
                                             size_t index);
struct pawn * PawnList_GetByIndex(struct pawn_list * list, 
                                  size_t index);


void PawnList_UpdatePawns(struct pawn_list * list, const struct map * map);

#endif // __PAWN_LIST_H__

