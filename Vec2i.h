#ifndef __VEC2I_H__
#define __VEC2I_H__
#include <math.h>

struct vec2i
{
   int x;
   int y;
};


static inline
struct vec2i * Vec2i_Set(struct vec2i * vec, int x, int y)
{
   vec->x = x;
   vec->y = y;
   return vec;
}

static inline
struct vec2i * Vec2i_SetByAngle(struct vec2i * vec, float angle_radians, float distance)
{
   vec->x = cos(angle_radians) * distance;
   vec->y = sin(angle_radians) * distance;
   return vec;
}

static inline
struct vec2i * Vec2i_Copy(struct vec2i * dest, const struct vec2i * src)
{
   dest->x = src->x;
   dest->y = src->y;
   return dest;
}

static inline
struct vec2i * Vec2i_Add(struct vec2i * dest, const struct vec2i * a, 
                                              const struct vec2i * b)
{
   dest->x = a->x + b->x;
   dest->y = a->y + b->y;
   return dest;
}

static inline
struct vec2i * Vec2i_Subtract(struct vec2i * dest, const struct vec2i * a,
                                                   const struct vec2i * b)
{
   dest->x = a->x - b->x;
   dest->y = a->y - b->y;
   return dest;
}

static inline
int Vec2i_ManhattenLength(const struct vec2i * vec)
{
   int x = vec->x;
   int y = vec->y;
   if(x < 0) x = -x;
   if(y < 0) y = -y;
   return x + y;
}

static inline
bool Vec2i_IsEqual(const struct vec2i * a, const struct vec2i * b)
{
   return a->x == b->x && a->y == b->y;
}


#endif // __VEC2I_H__

