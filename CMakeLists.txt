cmake_minimum_required(VERSION 3.1...3.28)


project(tactical-curses
        VERSION 0.0.1
        LANGUAGES C)


find_package(Curses REQUIRED)

add_executable(tactical-curses
   main.c
   Map.c
   MapRenderer.c
   Battle.c
   List.c
   Camera.c
   Pawn.c
   PawnList.c
   PawnListRenderer.c
)


if(MSVC)
  target_compile_options(tactical-curses PRIVATE /W4 /WX)
else()
  target_compile_options(tactical-curses PRIVATE -Wall -Wextra -Wpedantic -Werror)
endif()

target_include_directories(tactical-curses 
   PUBLIC
      ${CURSES_INCLUDE_DIR}
)

target_link_libraries(tactical-curses
   PUBLIC
      m
      ${CURSES_LIBRARIES}
)
   

