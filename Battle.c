#include <ncurses.h>
#include "Battle.h"
#include "Vec2i.h"
#include "MapRenderer.h"
#include "PawnListRenderer.h"
#include "Camera.h"


struct battle
{
   struct map * map;
   struct pawn_list * pawnList;
   struct camera camera;
};


static inline
void Battle_Draw(struct battle * battle)
{
   Camera_Update(&battle->camera);
   // Temp Code for testing
   Vec2i_Copy(&(PawnList_GetByIndex(battle->pawnList, 0)->look_at), &battle->camera.position);
  
   PawnList_UpdatePawns(battle->pawnList, battle->map);
   MapRenderer_Render(battle->map, &battle->camera);
   PawnListRenderer_Render(battle->pawnList, &battle->camera, 0);
   mvprintw(0, 0, "Pos %d, %d", battle->camera.position.x, battle->camera.position.y);
   move(battle->camera.screenCenter.y, battle->camera.screenCenter.x);
   refresh();
}

static inline
void Battle_HandleInput(struct battle * battle, int ch)
{
   struct vec2i pos = {0, 0};
   switch(ch)
   {
   case '8':
   case 'w':
      pos.y --;
      break;
   case '2':
   case 's':
      pos.y ++;
      break;
   case '4':
   case 'a':
      pos.x --;
      break;
   case '6':
   case 'd':
      pos.x ++;
      break;
   case '1':
      pos.x --;
      pos.y ++;
      break;
   case '3':
      pos.x ++;
      pos.y ++;
      break;
   case '7':
      pos.x --;
      pos.y --;
      break;
   case '9':
      pos.x ++;
      pos.y --;
      break;
   }
   
   Vec2i_Add(&pos, &pos, &battle->camera.position);

   if(pos.x < 0)
   {
      pos.x = 0;
   }
   else if (pos.x >= (int)battle->map->width)
   {
      pos.x = battle->map->width - 1;
   }
   if(pos.y < 0)
   {
      pos.y = 0;
   }
   else if (pos.y >= (int)battle->map->height)
   {
      pos.y = battle->map->height - 1;
   }
   Vec2i_Copy(&battle->camera.position, &pos);
}

void Battle_Start(struct map * map, struct pawn_list * pawnList)
{
   struct battle battleMem;
   struct battle * battle = &battleMem;

   battle->map = map;
   battle->pawnList = pawnList;
   Vec2i_Set(&battle->camera.position, 0, 0);

   Battle_Draw(battle);
   int ch = getch();
   clear();
   while(ch != 27 && ch != 'q')
   {
      Battle_HandleInput(battle, ch);
      Battle_Draw(battle);
      ch = getch();
      clear();
   }
}

