#include <stdio.h>
#include <ncurses.h>

#include "Map.h"
#include "PawnList.h"
#include "Battle.h"

static inline
void makeMap(struct map * map)
{
   const char * mapSrc = 
      "xxxxxxxxxx"
      "x        x"
      "x x      x"
      "x x      x"
      "x x      x"
      "x        x"
      "x      x x"
      "x      x x"
      "x      x x"
      "xxxxxxxxxx";

   for(size_t i = 0; i < map->size; i++)
   {
      char c = mapSrc[i];
      if(c == 'x')
      {
         map->data[i].isWall = true;
      }
   }
     
}

static inline
void test_loop()
{
   struct pawn_list * pawnList = PawnList_New();
   struct map map;
   Map_Init(&map, 10, 10);
   makeMap(&map);

   PawnList_Add(pawnList, 0, 8, 8); 
   PawnList_Add(pawnList, 1, 1, 1); 

   Battle_Start(&map, pawnList);

   Map_Destroy(&map);
   PawnList_Free(pawnList);
   
}


int main(int argc, char * args[])
{
   (void)argc;
   (void)args;

   initscr();

   raw();
   noecho();
   keypad(stdscr, TRUE);
   //curs_set(0); // Hide the cursor

   printw("Hello World !!!");
   refresh();
   test_loop();
   //getch();   

   endwin();

   printf("End of Program.\n");
   return 0;
}

