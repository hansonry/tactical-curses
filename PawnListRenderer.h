#ifndef __PAWNLISTRENDERER_H__
#define __PAWNLISTRENDERER_H__
#include "PawnList.h"
#include "Camera.h"

void PawnListRenderer_Render(const struct pawn_list * list, 
                             const struct camera * camera,
                             int teamView);


#endif // __PAWNLISTRENDERER_H__

