#ifndef __BATTLE_H__
#define __BATTLE_H__

#include "Map.h"
#include "PawnList.h"


void Battle_Start(struct map * map, struct pawn_list * pawnList);


#endif // __BATTLE_H__


