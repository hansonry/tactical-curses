#ifndef __MAPRENDERER_H__
#define __MAPRENDERER_H__

#include "Map.h"
#include "Camera.h"

void MapRenderer_Render(const struct map * map, const struct camera * camera);


#endif // __MAPRENDERER_H__

