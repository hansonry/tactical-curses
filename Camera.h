#ifndef __CAMERA_H__
#define __CAMERA_H__

#include "Vec2i.h"

struct camera
{
   struct vec2i position;
   struct vec2i screenCenter;
   int screenWidth;
   int screenHeight;
};

void Camera_Update(struct camera * camera);


#endif // __CAMERA_H__


