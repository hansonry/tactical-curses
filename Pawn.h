#ifndef __PAWN_H__
#define __PAWN_H__
#include <stdbool.h>
#include "Vec2i.h"
#include "Map.h"


struct pawnView
{
   bool * data;
   size_t sideLength;
   size_t center;
   size_t size;
   size_t dx;
   size_t dy;

};

struct pawn
{
   struct vec2i position;
   struct vec2i look_at;
   float fovRadians;
   int viewDistance;
   struct pawnView view;
   int team;
   int number;
};

void Pawn_Init(struct pawn * pawn);
void Pawn_Destroy(struct pawn * pawn);

void Pawn_UpdateView(struct pawn * pawn, const struct map * map);


#endif // __PAWN_H__

