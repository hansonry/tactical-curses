#include <stdlib.h>
#include <math.h>
#include "Pawn.h"


void Pawn_Init(struct pawn * pawn)
{
   struct pawnView * view = &pawn->view;
   pawn->fovRadians = 3.14 / 4.0f;
   pawn->viewDistance = 8;

   view->sideLength = pawn->viewDistance * 2 + 1;
   view->center = pawn->viewDistance;
   view->size = view->sideLength * view->sideLength;
   view->dx = 1;
   view->dy = view->sideLength;
   view->data = malloc(sizeof(bool) * view->size);
   
}

void Pawn_Destroy(struct pawn * pawn)
{
   free(pawn->view.data);
}

static inline
size_t Pawn_ViewIndexFromCoords(struct pawn * pawn, const struct vec2i * coords)
{
   
   struct pawnView * view = &pawn->view;
   int x = coords->x + view->center;
   int y = coords->y + view->center;
   return x * view->dx + y * view->dy;
}


static inline
void RayToPawn(struct pawn * pawn, const struct map * map, 
               const struct vec2i * start)
{
   struct pawnView * view = &pawn->view;
   //size_t index = Pawn_ViewIndexFromCoords(pawn, start);
   //view->data[index] = true;
   (void)map;
   int dx = start->x;
   int dy = start->y;
   if(dx < 0) dx = -dx;
   if(dy > 0) dy = -dy;
   int sx = start->x > 0 ? 1 : -1;
   int sy = start->y > 0 ? 1 : -1;
   int error = dx + dy;
   struct vec2i point = {0, 0};

   while(true)
   {
      size_t index = Pawn_ViewIndexFromCoords(pawn, &point);
      view->data[index] = true;

      if(Vec2i_IsEqual(&point, start)) break;
      int e2 = 2 * error;
      if(e2 >= dy)
      {
         if(point.x == start->x) break;
         error += dy;
         point.x += sx;
      }
      if(e2 <= dx)
      {
         if(point.y == start->y) break;
         error += dx;
         point.y += sy;
      }
      
   }
}

static inline
void Pawn_ClearView(struct pawn * pawn)
{
   struct pawnView * view = &pawn->view;
   size_t i;
   for(i = 0; i < view->size; i++)
   {
      view->data[i] = false;
   }
}

void Pawn_UpdateView(struct pawn * pawn, const struct map * map)
{
   Pawn_ClearView(pawn);

   // Find start and end points on arch
   struct vec2i lookDirection;
   Vec2i_Subtract(&lookDirection, &pawn->look_at, &pawn->position);
   float lookAngle = atan2(lookDirection.y, lookDirection.x);
   float halfFOV = pawn->fovRadians / 2.0f;
   float startAngle = lookAngle - halfFOV;
   float endAngle = lookAngle + halfFOV;
   if(endAngle < startAngle)
   {
      endAngle += 2 * 3.14;
   }
   float deltaAngle = atan2(0.5, pawn->viewDistance);
   float angle;
   for(angle = startAngle; angle <= endAngle; angle += deltaAngle)
   {
      struct vec2i point;
      float fx = cos(angle) * pawn->viewDistance + 0.5;
      float fy = sin(angle) * pawn->viewDistance + 0.5;
      Vec2i_Set(&point, fx, fy);
      RayToPawn(pawn, map, &point);
   }
}

