#include "List.h"
#include <stdlib.h>
#include <string.h>

static inline
void List_MakeRoomFor(struct list * list, size_t newSize, size_t growBy)
{
   if(newSize > list->size)
   {
      list->size = newSize + growBy;
      list->data = realloc(list->data, list->size * list->elementSize);
   }
}

void List_Init(struct list * list, 
               size_t elementSize, 
               size_t growBy, 
               size_t initSize)
{
   list->elementSize = elementSize;
   list->data = NULL;
   list->count = 0;
   if(growBy == 0)
   {
      list->growBy = 32; // default growby
   }
   else
   {
      list->growBy = growBy;
   }
   list->size = 0;
   if(initSize > 0)
   {
      List_MakeRoomFor(list, initSize, 0);
   }
}

void List_Free(struct list * list)
{
   if(list->data != NULL)
   {
      free(list->data);
      list->data = NULL;
   }
   list->elementSize = 0;
   list->size        = 0;
   list->count       = 0;
   list->growBy      = 0;
}

static inline
void * getPtr(const struct list * list, size_t index)
{
   return &list->data[index * list->elementSize];
}

static inline
void * addEmpty(struct list * list, size_t * indexPtr)
{
   size_t index = list->count;
   list->count ++;
   List_MakeRoomFor(list, list->count, list->growBy);
   if(indexPtr != NULL)
   {
      (*indexPtr) = index;
   }
   return getPtr(list, index);
}

void * List_AddEmpty(struct list * list, size_t * index)
{
   return addEmpty(list, index); 
}

void * List_AddCopy(struct list * list, const void * vp, size_t * index)
{
   void * ptr = addEmpty(list, index);
   memcpy(ptr, vp, list->elementSize);   
   return ptr;
}

static inline
bool isIndexValid(struct list * list, size_t index)
{
   return index < list->count;
}

static inline
bool swap(struct list * list, size_t aIndex, size_t bIndex)
{
   if(!isIndexValid(list, aIndex) || !isIndexValid(list, bIndex))
   {
      return false;
   }
   if(aIndex != bIndex)
   {
      void * temp, * aPtr, * bPtr;
      List_MakeRoomFor(list, list->count + 1, list->growBy);
      temp = getPtr(list, list->count);
      aPtr = getPtr(list, aIndex);
      bPtr = getPtr(list, bIndex);
      memcpy(temp, aPtr, list->elementSize);
      memcpy(aPtr, bPtr, list->elementSize);
      memcpy(bPtr, temp, list->elementSize);
   }
   return true;
}

bool List_Swap(struct list * list, size_t aIndex, size_t bIndex)
{
   return swap(list, aIndex, bIndex);
}

bool List_RemoveFast(struct list * list, size_t index)
{
   bool didSwap = swap(list, index, list->count - 1);
   if(!didSwap)
   {
      return false;
   }
   list->count --;
   return true;
}


