#ifndef __MAP_H__
#define __MAP_H__

#include <stdbool.h>
#include <stddef.h>

struct map_tile
{
   bool isWall;
};

struct map
{
   size_t width;
   size_t height;
   size_t dx;
   size_t dy;
   size_t size;
   struct map_tile * data;
};

void Map_Init(struct map * map, size_t width, size_t height);
void Map_Destroy(struct map * map);

bool Map_IsCoordInRange(const struct map * map, size_t x, size_t y);
bool Map_IsIndexInRage(const struct map * map, size_t index);
size_t Map_GetIndex(const struct map * map, size_t x, size_t y);

static inline
bool Map_DoesTileAtIndexBlocksVision(const struct map * map, size_t index)
{
   if(index < map->size)
   {
      return map->data[index].isWall;
   }
   return false;
}

#endif // __MAP_H__

