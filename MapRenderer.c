#include "MapRenderer.h"

#include <ncurses.h>

struct range
{
   int x;
   int y;
   int start_x;
   int start_y;
   int end_x;
   int end_y;
};

void MapRenderer_Render(const struct map * map, const struct camera * camera)
{
   struct range rmap, rscreen;
   rmap.start_x = camera->position.x - camera->screenCenter.x;
   if(rmap.start_x < 0)
   {
      rscreen.start_x = -rmap.start_x;
      rmap.start_x = 0;
   }
   else
   {
      rscreen.start_x = 0;
   }

   rmap.start_y = camera->position.y - camera->screenCenter.y;
   if(rmap.start_y < 0)
   {
      rscreen.start_y = -rmap.start_y;
      rmap.start_y = 0;
   }
   else
   {
      rscreen.start_y = 0;
   }

   rmap.end_x = rmap.start_x + camera->screenWidth;
   if(rmap.end_x >= (int)map->width)
   {
      rscreen.end_x = camera->screenWidth - (rmap.end_x - map->width);
      rmap.end_x = map->width - 1;
   }
   else
   {
      rscreen.end_x = camera->screenWidth;
   } 

   rmap.end_y = rmap.start_y + camera->screenHeight;
   if(rmap.end_y >= (int)map->height)
   {
      rscreen.end_y = camera->screenHeight - (rmap.end_y - map->height);
      rmap.end_y = map->height - 1;
   }
   else
   {
      rscreen.end_y = camera->screenHeight;
   } 

   // Start the Loop
   size_t startMapXIndex = Map_GetIndex(map, rmap.start_x, rmap.start_y);
   size_t mapIndex = startMapXIndex;
   rscreen.x = rscreen.start_x;
   rscreen.y = rscreen.start_y;
   rmap.x = rmap.start_x;
   rmap.y = rmap.start_y;
      

   while(rmap.x <= rmap.end_x && rmap.y <= rmap.end_y)
   {
      const struct map_tile * tile = &map->data[mapIndex];
      if(tile->isWall)
      {
         mvprintw(rscreen.y, rscreen.x, "#");
      }
      rscreen.x ++;
      rmap.x ++;
      mapIndex += map->dx;
      if(rmap.x > rmap.end_x)
      {
         rmap.x = rmap.start_x;
         rmap.y ++;
         rscreen.x = rscreen.start_x;
         rscreen.y ++;
         startMapXIndex += map->dy;
         mapIndex = startMapXIndex;
      }

   }
   
}

