#include "Map.h"

#include <stdlib.h>

static inline
void Map_Clear(struct map * map)
{
   size_t i;
   for(i = 0; i < map->size; i++)
   {
      map->data[i].isWall = false;
   }
}

void Map_Init(struct map * map, size_t width, size_t height)
{
   map->width = width;
   map->height = height;
   map->size = width * height;
   map->dx = 1;
   map->dy = width;
   map->data = malloc(sizeof(struct map_tile) * map->size);

   Map_Clear(map);
}


void Map_Destroy(struct map * map)
{
   map->width = 0;
   map->height = 0;
   map->size = 0;
   map->dx = 0;
   map->dy = 0;
   free(map->data);
   map->data = NULL;
}

bool Map_IsCoordInRange(const struct map * map, size_t x, size_t y)
{
   return x < map->width && y < map->height;
}

bool Map_IsIndexInRage(const struct map * map, size_t index)
{
   return index < map->size;
}

size_t Map_GetIndex(const struct map * map, size_t x, size_t y)
{
   return x * map->dx + y * map->dy;
}

